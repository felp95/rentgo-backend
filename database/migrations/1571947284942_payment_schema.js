'use strict'

const Schema = use('Schema')

class PaymentSchema extends Schema {
  up () {
    this.create('payments', (table) => {
      table.increments()
      table.integer('passenger_id').unsigned().notNullable().references('id').inTable('passengers').onUpdate('CASCADE').onDelete('CASCADE')
      table.integer('driver_id').unsigned().notNullable().references('id').inTable('drivers').onUpdate('CASCADE').onDelete('CASCADE')
      table.boolean('payment_status').notNullable().defaultTo(false)
      table.timestamps()
    })
  }

  down () {
    this.drop('payments')
  }
}

module.exports = PaymentSchema
