'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PassengerRatingsSchema extends Schema {
  up () {
    this.create('passenger_ratings', (table) => {
      table.increments()
      table.integer('driver_id').unsigned().notNullable().references('id').inTable('drivers')
      table.integer('passenger_id').unsigned().notNullable().references('id').inTable('passengers')
      table.integer('travel_done').unsigned().notNullable().references('id').inTable('trips')
      table.decimal('note').notNullable()
      table.string('observations')
      table.timestamps()
    })
  }

  down () {
    this.drop('passenger_ratings')
  }
}

module.exports = PassengerRatingsSchema
