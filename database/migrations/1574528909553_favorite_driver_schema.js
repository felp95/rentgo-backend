'use strict'

const Schema = use('Schema')

class FavoriteDriverSchema extends Schema {
  up () {
    this.create('favorite_drivers', (table) => {
      table.increments()
      table.integer('passenger_id').unsigned().notNullable().references('id').inTable('passengers').onUpdate('CASCADE').onDelete('CASCADE')
      table.integer('driver_id').unsigned().notNullable().references('id').inTable('drivers').onUpdate('CASCADE').onDelete('CASCADE')
      table.timestamps()
    })
  }

  down () {
    this.drop('favorite_drivers')
  }
}

module.exports = FavoriteDriverSchema
