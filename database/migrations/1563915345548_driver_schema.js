'use strict'

const Schema = use('Schema')

class DriverSchema extends Schema {
  up () {
    this.create('drivers', (table) => {
      table.increments()
      table.integer('user_id').unsigned().notNullable().references('id').inTable('users').onUpdate('CASCADE').onDelete('CASCADE')
      table.string('fullname').notNullable()
      table.string('mobile_phone').notNullable()
      table.string('profile_image')
      table.string('cnh_image')
      table.integer('dist_max').notNullable()
      table.enu('driver_status', ["traveling", "available", "unavailable"]).defaultTo("available")
      table.string('observations').notNullable()
      table.decimal('value_per_kilometer').notNullable()
      table.boolean('alcoholic_beverages').notNullable()
      table.boolean('animals').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('drivers')
  }
}

module.exports = DriverSchema
