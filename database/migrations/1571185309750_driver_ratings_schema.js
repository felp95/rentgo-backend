'use strict'

const Schema = use('Schema')

class DriverRatingsSchema extends Schema {
  up () {
    this.create('driver_ratings', (table) => {
      table.increments()
      table.integer('passenger_id').unsigned().notNullable().references('id').inTable('passengers')
      table.integer('driver_id').unsigned().notNullable().references('id').inTable('drivers')
      table.integer('travel_done').unsigned().notNullable().references('id').inTable('trips')
      table.decimal('note').notNullable()
      table.string('observations')
      table.timestamps()
    })
  }

  down () {
    this.drop('driver_ratings')
  }
}

module.exports = DriverRatingsSchema
