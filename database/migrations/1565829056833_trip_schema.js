'use strict'

const Schema = use('Schema')

class TripSchema extends Schema {
  up () {
    this.create('trips', (table) => {
      table.increments()
      table.integer('passenger_id').unsigned().notNullable().references('id').inTable('passengers').onUpdate('CASCADE').onDelete('CASCADE')
      table.integer('driver_id').unsigned().notNullable().references('id').inTable('drivers').onUpdate('CASCADE').onDelete('CASCADE')
      table.string('origin').notNullable()
      table.string('destination').notNullable()
      table.datetime('travel_date').notNullable()
      table.datetime('return_date')
      table.integer('number_passengers').notNullable()
      table.decimal('travel_price').notNullable()
      table.enu('travel_status', ['in_progress', 'waiting_driver', 'canceled', 'finished', 'scheduled'])
      table.integer('travel_distance')
      table.timestamps()
    })
  }

  down () {
    this.drop('trips')
  }
}

module.exports = TripSchema
