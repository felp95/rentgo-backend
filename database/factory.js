"use strict";

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use("Factory");

Factory.blueprint("App/Models/User", async (faker) => {
    return {
        username: faker.name(),
        email: faker.email(),
        password: "123456",
    };
});

Factory.blueprint("App/Models/Passenger", async (faker) => {
    return {
        user_id: async () => {
            return (await Factory.model("App/Models/User").create()).id;
        },
        fullname: async () => {
            return (await Factory.model("App/Models/User").create()).username;
        },
        mobile_phone: faker.phone(),
        profile_image: faker.avatar({ protocol: "https" }),
    };
});

Factory.blueprint("App/Models/Driver", async (faker) => {
    return {
        user_id: async () => {
            return (await Factory.model("App/Models/User").create()).id;
        },
        fullname: async () => {
            return (await Factory.model("App/Models/User").create()).username;
        },
        mobile_phone: faker.phone({ country: "br" }),
        profile_image: faker.avatar({ protocol: "https" }),
        dist_max: faker.integer({ min: 50, max: 500 }),
        observations:
            "Viagens acima de 150km será cobrado valor de alimentação",
        value_per_kilometer: faker.integer({ min: 2, max: 50 }),
        alcoholic_beverages: false,
        animals: false,
    };
});
