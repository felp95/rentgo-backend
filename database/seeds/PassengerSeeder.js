"use strict";

/*
|--------------------------------------------------------------------------
| PassengerSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use("Factory");

class PassengerSeeder {
    async run() {
        await Factory.model("App/Models/Passenger").createMany(10);
    }
}

module.exports = PassengerSeeder;
