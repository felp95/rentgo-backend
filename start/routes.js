"use strict";

const Route = use("Route");

Route.get("/", ({ view }) => {
    return view.render("home.index");
});

Route.post("/api/authentication", "AuthenticationController.store").validator(
    "Auth"
);
Route.post("/api/forgot-password", "ForgotPasswordController.store");
Route.put("/api/reset-password", "ForgotPasswordController.update");

Route.post("/api/user", "UserController.store");
Route.put("/api/user/:id", "UserController.update");

Route.post("/api/upload", "FileController.store");

Route.group(() => {
    /**
     * Van routes
     */
    Route.resource("/van", "VanController").apiOnly();

    /**
     * Passenger routes
     */
    Route.resource("/passenger", "PassengerController").apiOnly();

    /**
     * Driver routes
     */
    Route.resource("/driver", "DriverController").apiOnly();

    /**
     * Driver auth route
     */
    Route.resource("/driver-auth", "DriverAuthenticatedController").apiOnly();

    /**
     * Passenger auth route
     */
    Route.resource(
        "/passenger-auth",
        "PassengerAuthenticatedController"
    ).apiOnly();

    /**
     * Trip routes
     */
    Route.resource("/trip", "TripController").apiOnly();

    /**
     * Driver trips routes
     */
    Route.resource("/driver-trips", "DriverTripController").apiOnly();

    /**
     * Passenger trips routes
     */
    Route.resource("/passenger-trips", "PassengerTripController").apiOnly();

    /**
     * Notification player_id routes
     */
    Route.resource("/notification", "NotificationController").apiOnly();

    /**
     * Driver filters
     */
    Route.resource("/driver-filters", "DriverFilterController").apiOnly();

    /**
     * Rating driver
     */
    Route.resource("/driver-rating", "DriverRatingController").apiOnly();

    /**
     * Rating passenger
     */
    Route.resource("/passenger-rating", "PassengerRatingController").apiOnly();

    /**
     * Payment
     */
    Route.resource("/payment", "PaymentController").apiOnly();

    /**
     * Driver money raised
     */
    Route.resource("/driver-money", "DriverTripMoneyController").apiOnly();

    /**
     * Driver van
     */
    Route.resource("/driver-van", "DriverVanController").apiOnly();

    /**
     * Drivers with filters
     */
    Route.resource("/driver-filter", "FilterController").apiOnly();

    Route.get("/favoriteDriver", "FavoriteDriverController.index");
    Route.post("/favoriteDriver", "FavoriteDriverController.store");
    Route.delete("/favoriteDriver", "FavoriteDriverController.destroy");
})
    .prefix("/api")
    .middleware(["auth"]);
