const User = use("App/Models/User");

class UserServiceFactory {
    constructor(data) {
        this.data = data;
    }

    create() {
        return UserService(this.data);
    }
}

async function UserService(data) {
    const user = await User.create(data);

    return user;
}

module.exports = UserServiceFactory;
