'use strict'

const Model = use('Model')

class PassengerRating extends Model {
    passenger () {
        return this.belongsTo('App/Models/Passenger')
    }

    driver () {
        return this.belongsTo('App/Models/Driver')
    }
}

module.exports = PassengerRating
