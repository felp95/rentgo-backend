'use strict'

const Env = use('Env')
const Database = use('Database')
const axios = require('axios')
const PaymentHook = exports = module.exports = {}

PaymentHook.sendPaymentPush = async (modelInstance) => {

    const response = await Database
        .select("player_id")
        .from("drivers")
        .leftJoin("users", " users.id", "drivers.user_id")
        .leftJoin("notifications", "users.id", "notifications.user_id")
        .where("drivers.id", modelInstance.driver_id)

    const passenger = await Database
        .select("fullname")
        .from("passengers")
        .where("passengers.id", modelInstance.passenger_id)


    await axios({
        method: 'POST',
        url: 'https://onesignal.com/api/v1/notifications',
        headers: {
            'Authorization' : `Basic ${Env.get("ONESIGNAL_TOKEN")}`
        },
        data: {
            "app_id" : `${Env.get("ONESIGNAL_APPID")}`,
            "contents": {
                "en": `O passageiro ${passenger[0].fullname} realizou o pagamento. A corrida pode ser iniciada!`
            },
            "headings": {
                "en": "RentGo"
            },
            "include_player_ids": [`${response[0].player_id}`],
            "buttons": [
                {
                    "id": "1",
                    "text": "Visualizar"
                }
            ],
        }
    })
}
