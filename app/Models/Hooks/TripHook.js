'use strict'

const Env = use('Env')
const Database = use('Database')
const axios = require('axios')
const moment = require('moment')
const TripHook = exports = module.exports = {}

TripHook.sendPushNotification = async (modelInstance) => {

    const response = await Database
                                    .select("player_id")
                                    .from("drivers")
                                    .leftJoin("users"," users.id", "drivers.user_id")
                                    .leftJoin("notifications", "users.id", "notifications.user_id")
                                    .where("drivers.id", modelInstance.driver_id)
    
    const passenger = await Database
                                    .select("fullname")
                                    .from("passengers")
                                    .where("passengers.id", modelInstance.passenger_id)

    const travelDate = moment(modelInstance.travel_date).format('DD/MM/YYYY HH:mm')

    await axios({
        method: 'POST',
        url: 'https://onesignal.com/api/v1/notifications',
        headers: {
            'Authorization' : `Basic ${Env.get("ONESIGNAL_TOKEN")}`
        },
        data: {
            "app_id" : `${Env.get("ONESIGNAL_APPID")}`,
            "data": {
                "passenger_id" : `${modelInstance.passenger_id}`,
                "trip_id" : `${modelInstance.id}`,
                "travel_price" : `${modelInstance.travel_price}`,
                "passenger" : `${passenger[0].fullname}`,
                "number_passengers" : `${modelInstance.number_passengers}`,
                "origin" : `${modelInstance.origin}`,
                "destination" : `${modelInstance.destination}`,
                "travel_date" : `${travelDate}`
            },
            "contents" : {
                "en" : `Destino: ${modelInstance.destination}` + "\n\n" + `Passageiro: ${passenger[0].fullname}`
            },
            "headings" : {
                "en" : "RentGo - Solicitação de viagem"  
            },
            "include_player_ids": [`${response[0].player_id}`],
            "buttons": [
                {
                    "id": "1",
                    "text": "Aceitar"
                },
                {
                    "id": "2",
                    "text": "Recusar"
                }
            ],
        }
    })
}