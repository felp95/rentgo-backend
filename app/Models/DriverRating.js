'use strict'

const Model = use('Model')

class DriverRating extends Model {
    driver () {
        return this.belongsTo('App/Models/Driver')
    }

    passenger () {
        return this.belongsTo('App/Models/Passenger')
    }
}

module.exports = DriverRating
