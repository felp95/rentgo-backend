'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class FavoriteDriver extends Model {
    passenger()
    {
        return this.belongsTo('App/Models/Passenger')
    }

    driver()
    {
        return this.belongsTo('App/Models/Driver')
    }
}

module.exports = FavoriteDriver
