"use strict";

const Model = use("Model");

class Trip extends Model {
    static boot() {
        super.boot();

        this.addHook("beforeCreate", "TripHook.sendPushNotification");
    }

    static get hidden() {
        return ["created_at", "updated_at"];
    }

    driver() {
        return this.belongsTo("App/Models/Driver");
    }

    passenger() {
        return this.belongsTo("App/Models/Passenger");
    }
}

module.exports = Trip;
