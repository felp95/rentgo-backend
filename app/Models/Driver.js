'use strict'

const Model = use('Model')

class Driver extends Model {	

	static get hidden () {
        return ['created_at', 'updated_at', 'cnh_image']
    }

	user () {
		return this.belongsTo('App/Models/User')
	}

	vans () {
		return this.hasMany('App/Models/Van')
	}

	trip () {
		return this.hasMany('App/Models/Trip')
	}

	rating () {
		return this.hasMany('App/Models/DriverRating')
	}
}

module.exports = Driver
