'use strict'

const Model = use('Model')

class Passenger extends Model {
    user () {
		return this.belongsTo('App/Models/User')
	}

	rating () {
		return this.hasMany('App/Models/PassengerRating')
	}

	driver_favorited(){
		return this.belongsToMany('App/Models/Driver')
		.pivotTable('favorite_drivers')
	}
}

module.exports = Passenger
