'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Payment extends Model {

    static boot() {
        super.boot()

        this.addHook('beforeSave', 'PaymentHook.sendPaymentPush')
    }

    passenger () {
        return this.belongsTo('App/Models/Passenger')
    }

    driver () {
        return this.belongsTo('App/Models/Driver')
    }

}

module.exports = Payment
