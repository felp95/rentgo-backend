'use strict'

const Model = use('Model')

class Van extends Model {

    static get hidden () {
        return ['created_at', 'updated_at']
    }

    driver () {
        return this.belongsTo('App/Models/Driver')
    }
}

module.exports = Van
