'use strict'

const Model = use('Model')

class Notification extends Model {

	static get hidden () {
        return ['id', 'user_id', 'created_at', 'updated_at']
    }

    user () {
		return this.belongsTo('App/Models/User')
	}
}

module.exports = Notification
