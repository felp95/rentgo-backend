'use strict'

const Antl = use('Antl')

class Driver {
  get validateAll() {
    return true
  }

  get rules () {
    return {
      fullname: 'required',
      mobile_phone: 'required',
      dist_max: 'required'
    }
  }

  get messages () {
    return Antl.list('validation')  
  }
}

module.exports = Driver
