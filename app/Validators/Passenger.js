'use strict'

const Antl = use('Antl')

class Passenger {
  get validateAll() {
    return true
  }

  get rules () {
    return {
      fullname: 'required',
      mobile_phone: 'required'
    }
  }

  get messages () {
    return Antl.list('validation')  
  }
}

module.exports = Passenger
