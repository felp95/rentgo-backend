"use strict";

class Auth {
    get validateAll() {
        return true;
    }

    get rules() {
        return {
            email: "required|email",
            password: "required"
        };
    }

    get messages() {
        return {
            "email.required": "O e-mail é obrigatório.",
            "password.required": "A senha é obrigatória",

            "email.email": "Informe um endereço de e-mail válido"
        };
    }
}

module.exports = Auth;
