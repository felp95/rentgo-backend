'use strict'

const Antl = use('Antl')

class Van {
  get validateAll() {
    return true
  }

  get rules () {
    return {
      model: 'required',
      color: 'required',
      plate: 'required',
      amount_passenger: 'required',
      details: 'required'
    }
  }

  get messages () {
    return Antl.list('validation')  
  }
}

module.exports = Van
