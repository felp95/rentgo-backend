"use strict";

const Raven = require("raven");
const Config = use("Config");
const BaseExceptionHandler = use("BaseExceptionHandler");

class ExceptionHandler extends BaseExceptionHandler {
    async handle(error, { request, response }) {
        if (error.name === "UserNotFoundException") {
            return response.status(error.status).send({
                message: "Usuário não encontrado.",
            });
        }

        if (error.name === "PasswordMisMatchException") {
            return response.status(error.status).send({
                message: "Usuário não encontrado.",
            });
        }

        if (error.name === "ValidationException") {
            return response.status(error.status).send(error.messages);
        }

        if (error.name === "InvalidJwtToken") {
            return response.status(error.status).send({
                message: "Faça o login novamente.",
            });
        }
    }

    async report(error, { request }) {
        Raven.config(Config.get("services.sentry.dsn"));
        Raven.captureException(error);
    }
}

module.exports = ExceptionHandler;
