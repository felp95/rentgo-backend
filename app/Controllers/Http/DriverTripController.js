'use strict'

const Trip = use('App/Models/Trip')

class DriverTripController {

    async index ({ request, response }) {
        try {
            const { status, driver } = request.get()

            const driverTrips = await Trip.query().where('travel_status', status).andWhere('driver_id', driver).orderBy('created_at', 'desc').with('passenger', builder => {
                builder.select('id', 'user_id', 'fullname', 'mobile_phone', 'profile_image')
            }).fetch()

            return response.json({
                result: driverTrips
            })
        } catch (error) {
            return error.message
        }
    }
}

module.exports = DriverTripController
