'use strict'

const PassengerRating = use('App/Models/PassengerRating')

class PassengerRatingController {

	async index ({ request, response, view }) {
	}

	async store ({ request, response }) {
		try {
			const data = request.only([
				'driver_id',
				'passenger_id',
				'travel_done',
				'note',
				'observations'
			])

			const rating = await PassengerRating.create(data)

			return response.json({
				status: 'attributed',
				result: rating
			})

		} catch (error) {
			return error.message
		}
	}

	async show ({ params, request, response }) {
		try {
			const positiveNotes = await PassengerRating.query().count('note').where('note', '>', '3').andWhere('passenger_id', params.id)
			const negativeNotes = await PassengerRating.query().count('note').where('note', '<=', '3').andWhere('passenger_id', params.id)

			const formtaPositiveNotes = parseInt(positiveNotes[0].count)
			const formatNegativeNotes = parseInt(negativeNotes[0].count)

			const totalNotes = await PassengerRating.query().where('passenger_id', params.id).whereBetween('note', [0,5]).count()

			const notes = await PassengerRating.query().where('passenger_id', params.id).with('passenger', builder => {
				builder.select('fullname')
			}).with('driver', builder => {
				builder.select('fullname')
			}).fetch()

			const formatData = notes.toJSON()

			const sumNotes = formatData.reduce((total, valor) => total + parseFloat(valor.note), 0)

			const total = totalNotes[0].count

			const media = sumNotes / parseFloat(total)

			return response.json({
				status: 'ok',
				media: parseFloat(media.toFixed(2)),
				positive: formtaPositiveNotes,
				negative: formatNegativeNotes
			})
			
		} catch (error) {
			return error
		}
	}

	async update ({ params, request, response }) {
	}

	async destroy ({ params, request, response }) {
	}
}

module.exports = PassengerRatingController