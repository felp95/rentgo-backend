'use strict'

const Van = use('App/Models/Van')

class DriverVanController {

    async index({ request, response }) {
		try {

            const { driver } = request.get() 

			const driverVans = await Van.query().where('driver_id', driver).with('driver', builder => {
                builder.select('id', 'fullname')
            }).fetch()

			return response.json({
				driverVans
			})
		} catch (error) {
			return error.message
		}
	}
}

module.exports = DriverVanController
