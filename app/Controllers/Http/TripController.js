"use strict";

const Trip = use("App/Models/Trip");

class TripController {
    async index({ request, response }) {
        try {
            const { page } = request.get();

            const trips = await Trip.query()
                .orderBy("trips.id", "desc")
                .paginate(page, 10);

            return response.json({
                status: "ok",
                result: trips,
            });
        } catch (error) {
            return error.message;
        }
    }

    async store({ request, response }) {
        try {
            const data = request.only([
                "origin",
                "destination",
                "travel_date",
                "return_date",
                "number_passengers",
                "travel_distance",
                "passenger_id",
                "driver_id",
                "travel_price",
            ]);

            const trip = await Trip.create({
                ...data,
                travel_status: "waiting_driver",
            });

            return response.json({
                status: "created",
                result: trip,
            });
        } catch (error) {
            return error.message;
        }
    }

    async show({ params, response }) {
        try {
            const trip = await Trip.query()
                .where("trips.id", "=", params.id)
                .with("passenger", (builder) => {
                    builder.select(
                        "id",
                        "user_id",
                        "fullname",
                        "mobile_phone",
                        "profile_image"
                    );
                    builder.with("user", (builder) => {
                        builder.select("id", "username", "email");
                        builder.with("notification");
                    });
                })
                .fetch();

            return response.json({
                status: "ok",
                result: trip,
            });
        } catch (error) {
            return error.message;
        }
    }

    async update({ params, request, response }) {
        try {
            const trip = await Trip.findOrFail(params.id);

            const data = request.only(["travel_status"]);

            trip.merge(data);

            await trip.save();

            return response.json({
                status: "updated",
                result: trip,
            });
        } catch (error) {
            return error;
        }
    }
}

module.exports = TripController;
