"use strict";

const Driver = use("App/Models/Driver");

class DriverController {
    async index({ request, response }) {
        try {
            const { dist_max, page, status } = request.all();

            const drivers = await Driver.query()
                .where("dist_max", ">=", dist_max)
                .andWhere("driver_status", status)
                .with("trip", (builder) => {
                    builder.where("travel_status", "finished");
                })
                .paginate(page, 10);

            return response.json({
                status: "ok",
                drivers,
            });
        } catch (error) {
            return error.message;
        }
    }

    async store({ request, response, auth }) {
        try {
            const data = request.only([
                "fullname",
                "mobile_phone",
                "profile_image",
                "cnh_image",
                "dist_max",
                "observations",
                "value_per_kilometer",
                "alcoholic_beverages",
                "air_conditioning",
                "television",
                "animals",
            ]);

            const driver = await Driver.create({
                ...data,
                user_id: auth.user.id,
            });

            return response.json({
                status: "created",
                result: driver,
            });
        } catch (error) {
            return error.message;
        }
    }

    async show({ params, response }) {
        try {
            const driver = await Driver.query()
                .where("drivers.id", params.id)
                .with("trip", (builder) => {
                    builder.where("travel_status", "finished");
                })
                .with("user", (builder) => {
                    builder.select("id", "username", "email");
                    builder.with("notification");
                })
                .fetch();

            return response.json({
                status: "ok",
                driver,
            });
        } catch (error) {
            return error.message;
        }
    }

    async update({ params, request, response }) {
        try {
            const driver = await Driver.findOrFail(params.id);
            const data = request.only([
                "fullname",
                "mobile_phone",
                "profile_image",
                "mobile_phone",
                "dist_max",
                "driver_status",
                "rating",
                "positive_notes",
                "negative_notes",
            ]);

            driver.merge(data);

            await driver.save();

            return response.json({
                status: "updated",
                result: driver,
            });
        } catch (error) {
            return error.message;
        }
    }
}

module.exports = DriverController;
