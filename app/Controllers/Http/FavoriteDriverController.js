"use strict";

const FavoriteDriver = use("App/Models/FavoriteDriver");

class FavoriteDriverController {
    async index({ request, response }) {
        const { passenger_id, page } = request.get();

        const listFavorite = await FavoriteDriver.query()
            .select("drivers.id", "drivers.fullname", "drivers.profile_image")
            .leftJoin("drivers", "drivers.id", "favorite_drivers.driver_id")
            .leftJoin(
                "driver_ratings",
                "driver_ratings.driver_id",
                "drivers.id"
            )
            .where("favorite_drivers.passenger_id", passenger_id)
            .paginate(page, 5);

        return response.json({
            status: 200,
            message: "ok",
            result: listFavorite,
        });
    }

    async store({ request, response, auth }) {
        const data = request.only(["passenger_id", "driver_id"]);

        const favoriteDriver = await FavoriteDriver.create(data);

        return response.json({
            status: 201,
            message: "created",
            result: favoriteDriver,
        });
    }

    async destroy({ request, response }) {
        const { driver_id } = request.get();
        const { passenger_id } = request.get();

        const rowDelete = await FavoriteDriver.query()
            .from("favorite_drivers")
            .where({ driver_id: driver_id, passenger_id: passenger_id })
            .delete();

        return response.json({
            status: 204,
            message: "removed",
            result: rowDelete,
        });
    }
}

module.exports = FavoriteDriverController;
