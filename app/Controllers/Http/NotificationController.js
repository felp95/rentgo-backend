'use strict'

const Notification = use('App/Models/Notification')

class NotificationController {

	async store ({ request, response, auth }) {
		try {
			const { player_id } = request.all()

			const player = await Notification.findBy('player_id', player_id)

			if(player === null) {

				const playerId = await Notification.create({
					player_id: player_id,
					user_id: auth.user.id
				})
	
				return response.json({
					status: 'created',
					result: playerId
				})
			}
			
			return response.json({
				status: 'Player Id já cadastrado'
			})

		} catch (error) {
			return error.message
		}
	}
}

module.exports = NotificationController
