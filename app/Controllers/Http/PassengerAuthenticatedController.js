'use strict'

const Passenger = use('App/Models/Passenger')

class PassengerAuthenticatedController {

    async index({ response, auth }) {
        try {
            const passenger = await Passenger
                                            .query()
                                            .select('passengers.id', 'username', 'email', 'fullname', 'mobile_phone', 'profile_image')
                                            .from('passengers')
                                            .leftJoin('users', 'passengers.user_id', 'users.id')
                                            .where('users.id', '=', auth.user.id)
                                            .fetch()

            return response.json({
                status: 'ok',
                result: passenger
            })

        } catch (error) {
            return error.message
        }
    }
}

module.exports = PassengerAuthenticatedController
