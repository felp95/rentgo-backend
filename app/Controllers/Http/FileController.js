'use strict'

const Drive = use('Drive')
const File = use('App/Models/File')

class FileController {

	async index ({ request, response, view }) {
	}

	async store ({ request, response }) {
		await request.multipart.file('image', {}, async (file) => {
			try {
				const ContentType = file.headers['content-type']
				const ACL = 'public-read'
				const Key = `${(Math.random() * 100).toString(32)}-${file.clientName}`

				const url = await Drive.put(Key, file.stream, {
					ContentType,
					ACL,
				})

				await File.create({
					name: file.clientName,
					key: Key,
					url,
					content_type: ContentType
				})

			} catch (error) {
				return error.message
			}
		}).process()
	}

	async show ({ params, request, response, view }) {
	}

	async update ({ params, request, response }) {
	}

	async destroy ({ params, request, response }) {
	}
}

module.exports = FileController