'use strict'

const Driver = use('App/Models/Driver')
const DriverRating = use('App/Models/DriverRating')

class DriverRatingController {

	async store ({ request, response }) {
		try {
			const data = request.only([
				'passenger_id',
				'driver_id',
				'travel_done',
				'note',
				'observations'
			])

			const rating = await DriverRating.create(data)

			return response.json({
				status: 'attributed',
				result: rating
			})

		} catch (error) {
			return error.message
		}
	}

	async show ({ params, request, response }) {
		try {
			const positiveNotes = await DriverRating.query().count('note').where('note', '>', '3').andWhere('driver_id', params.id)
			const negativeNotes = await DriverRating.query().count('note').where('note', '<=', '3').andWhere('driver_id', params.id)

			const formtaPositiveNotes = parseInt(positiveNotes[0].count)
			const formatNegativeNotes = parseInt(negativeNotes[0].count)

			const totalNotes = await DriverRating.query().where('driver_id', '=', params.id).whereBetween('note', [0,5]).count()
		
			const notes = await DriverRating.query().where('driver_id', params.id).with('passenger', builder => {
				builder.select('fullname')
			}).with('driver', builder => {
				builder.select('fullname')
			}).fetch()

			const formatData = notes.toJSON()

			const sumNotes = formatData.reduce((total, valor) => total + parseFloat(valor.note), 0)

			const total = totalNotes[0].count

			const media = sumNotes / parseFloat(total)

			return response.json({
				status: 'ok',
				media: parseFloat(media.toFixed(2)),
				positive: formtaPositiveNotes,
				negative: formatNegativeNotes
			})
			
		} catch (error) {
			
		}
	}
}

module.exports = DriverRatingController