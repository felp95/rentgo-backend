'use strict'

const Payment = use('App/Models/Payment')

class PaymentController {

	async index ({ request, response, view }) {	
	}

	async store ({ request, response }) {
		try {
			const data = request.only([
				'passenger_id',
				'driver_id',
				'payment_status'
			])

			const payment = await Payment.create(data)


			return response.json({
				status: 'created',
				payment
			})
		} catch (error) {
			return error.message
		}

	}

	async show ({ params, request, response, view }) {
	}

	async update ({ params, request, response }) {
	}

	async destroy ({ params, request, response }) {
	}
}

module.exports = PaymentController
