'use strict'

const Van = use('App/Models/Van')

class VanController {

	async index ({ request, response }) {
		try {
			const { driver, page } = await request.all()

			const vans = await Van
								.query()
								.select('vans.id', 'driver_id', 'drivers.fullname as driver', 'model', 'color', 'amount_passenger', 'plate', 'details')
								.leftJoin('drivers', 'drivers.id', 'vans.driver_id')
								.where('driver_id', '=', driver)
								.orderBy('vans.id', 'desc')
								.paginate(page, 10)

			return response.json({
				status: 'ok',
				result: vans
			})
		} catch (error) {
			return error.message
		}
	}
	

	async store ({ request, response }) {
		try {

			const { driver } = await request.all()

			const data = request.only([
				'model',
				'color',
				'plate',
				'amount_passenger',
				'details',
				'photo_van',
				'air_conditioning',
				'television'
			])

			const van = await Van.create({
				...data,
				driver_id: driver
			})

			return response.json({
				status: 'created',
				result: van
			})
		} catch (error) {
			return error.message
		}
	}

	async show ({ params, response }) {
		try {
			const van = await Van.findOrFail(params.id)

			return response.json({
				status: 'ok',
				result: van
			})
		} catch (error) {
			return error.message
		}
	}

	async update ({ params, request, response }) {
		try {
			const van = await Van.findOrFail(params.id)
			const data = request.only([
				'details',
				'photo_van'
			])

			van.merge(data)

			await van.save()

			return response.json({
				status: 'updated',
				result: van
			})

		} catch (error) {
			return error.message
		}
	}
	
	async destroy ({ params, response }) {
		try {
			const van = await Van.findOrFail(params.id)

			van.delete()

			return response.json({
				status: 'deleted'
			})
		} catch (error) {
			return error.message
		}
	}
}

module.exports = VanController
