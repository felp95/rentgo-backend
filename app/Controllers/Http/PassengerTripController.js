"use strict";

const Trip = use("App/Models/Trip");

class PassengerTripController {
    async index({ request, response }) {
        try {
            const { status, passenger, page } = request.get();

            const passengerTrips = await Trip.query()
                .where("travel_status", status)
                .andWhere("passenger_id", passenger)
                .with("driver", (builder) => {
                    builder.select("*");
                })
                .paginate(page, 10);

            return response.json({
                status: 200,
                message: "ok",
                result: passengerTrips,
            });
        } catch (error) {
            return error.message;
        }
    }
}

module.exports = PassengerTripController;
