'use strict'

const Driver = use('App/Models/Driver')

class DriverAuthenticatedController {

    async index({ response, auth }) {
        try {
            const driver = await Driver
                                    .query()
                                    .select('drivers.id', 'username', 'email', 'fullname', 'mobile_phone', 'profile_image', 'dist_max')
                                    .from('drivers')
                                    .leftJoin('users', 'drivers.user_id', 'users.id')
                                    .where('users.id', '=', auth.user.id)
                                    .fetch()

            return response.json({
                status: 'ok',
                result: driver
            })

        } catch (error) {
            return error.message
        }
    }

}

module.exports = DriverAuthenticatedController
