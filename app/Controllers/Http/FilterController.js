'use strict'

const Driver = use("App/Models/Driver")

class FilterController {

    async index({ request, response }) {
        try {
            const { animal, air_conditioning, television, alcoholic_beverages } = request.all()

            if(animal && alcoholic_beverages && air_conditioning && television) {
                const driver = await Driver.query().where('animals', animal).andWhere('alcoholic_beverages', alcoholic_beverages).with('vans', builder => {
                    builder.where('air_conditioning', air_conditioning)
                    builder.where('television', television)
                }).fetch()

                return response.json(driver)
            }

            if(animal && alcoholic_beverages && air_conditioning) {
                const driver = await Driver.query().where('animals', animal).andWhere('alcoholic_beverages', alcoholic_beverages).with('vans', builder => {
                    builder.where('air_conditioning', air_conditioning)
                }).fetch()

                return response.json(driver)
            }

            if(air_conditioning && television) {
                const driver = await Driver.query().with('vans', builder => {
                    builder.where('television', television)
                    builder.where('air_conditioning', air_conditioning)
                }).fetch()

                return response.json(driver)
            }

            if(alcoholic_beverages && television) {
                const driver = await Driver.query().where('alcoholic_beverages', alcoholic_beverages).with('vans', builder => {
                    builder.where('television', television)
                }).fetch()

                return response.json(driver)
            }

            if(alcoholic_beverages && air_conditioning) {
                const driver = await Driver.query().where('alcoholic_beverages', alcoholic_beverages).with('vans', builder => {
                    builder.where('air_conditioning', air_conditioning)
                }).fetch()

                return response.json(driver)
            }

            if(animal && television) {
                const driver = await Driver.query().where('animals', animal).with('vans', builder => {
                    builder.where('television', television)
                }).fetch()

                return response.json(driver)
            }

            if(animal && air_conditioning) {
                const driver = await Driver.query().where('animals', animal).with('vans', builder => {
                    builder.where('air_conditioning', air_conditioning)
                }).fetch()

                return response.json(driver)
            }

            if(animal && alcoholic_beverages) {
                const driver = await Driver.query().where('animals', animal).andWhere('alcoholic_beverages', alcoholic_beverages).fetch()

                return response.json(driver)
            }

            if(television) {
                const driver = await Driver.query().with('vans', builder => {
                    builder.where('television', television)
                }).fetch()

                return response.json(driver)
            }

            if(air_conditioning) {
                const driver = await Driver.query().with('vans', builder => {
                    builder.where('air_conditioning', air_conditioning)
                }).fetch()

                return response.json(driver)
            }

            if(alcoholic_beverages) {
                const driver = await Driver.query().where('alcoholic_beverages', alcoholic_beverages).fetch()

                return response.json(driver)
            }

            if(animal) {
                const driver = await Driver.query().where('animals', animal).fetch()

                return response.json(driver)
            }
            
        } catch (error) {
            return error.message
        }
    }
}

module.exports = FilterController
