'use strict'

const Trip = use('App/Models/Trip')

class DriverTripMoneyController {

    async show ({ params, request, response }) {
        try {
            const tripsDriver = await Trip.query().select('travel_price').where('driver_id', params.id).andWhere('travel_status', 'finished').fetch()
            const newData = tripsDriver.toJSON()

            const res = newData.reduce((total, valor) => {
                return total + (parseFloat(valor.travel_price) * 0.95)
            }, 0)

            return response.json({
                driver_money: res
            })
        } catch (error) {
            return error.message
        }
    }
}

module.exports = DriverTripMoneyController
